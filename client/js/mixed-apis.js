(function() {
    'use strict';
    angular.module('MixedApis', ['ngCookies', 'ngResource', 'ngAnimate', 'ui.router', 'UiRouterMenuService']);
}());
