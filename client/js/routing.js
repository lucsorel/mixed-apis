(function() {
    'use strict';
    angular.module('MixedApis').config(Routing);

    Routing.$inject = ['$stateProvider', '$urlRouterProvider'];
    function Routing($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('root', {
                abstract: true,
                url: '/',
                views: {
                    menu: {
                        templateUrl: 'templates/menu.html',
                        controller: 'MenuController as menuCtrl'
                    }
                }
            })
            .state('root.public', {
                url: 'public',
                resolve: {
                    page: ['menuService', function(menuService) {
                        return menuService.setViewName('public');
                    }]
                },
                views: {
                    public: {
                        templateUrl: 'templates/public.html'
                    }
                }
            })
            .state('root.auth', {
                abstract: true,
                url: 'auth',
                // defines the ui-view endpoint for authenticated content
                views: {
                    auth: {}
                }
            })
            // login state
            .state('root.auth.login', {
                url: '/login/:nextState',
                views: {
                    login: {
                        templateUrl: 'templates/auth-login.html',
                        controller: 'LoginController as loginCtrl'
                    }
                }
            })
            .state('root.auth.timeseries', {
                url: '/timeseries',
                resolve: {
                    page: ['menuService', function(menuService) {
                        return menuService.setViewName('auth-timeseries');
                    }],
                    // redirects to the login page if logged out
                    user: ['$state', 'Auth', function($state, Auth) {
                        return Auth.getUserOrLogin('root.auth.timeseries');
                    }]
                },
                views: {
                    home: {
                        templateUrl: 'templates/auth-timeseries.html',
                        controller: 'TimeseriesController as tsCtrl'
                    },
                    // adds the user-related entries in the menu
                    'user@root': {
                        templateUrl: 'templates/user-menu.html',
                        controller: 'UserController as userCtrl'
                    }
                }
            })
            .state('root.auth.notifications', {
                url: '/notifications',
                resolve: {
                    page: ['menuService', function(menuService) {
                        return menuService.setViewName('auth-notifications');
                    }],
                    // redirects to the login page if logged out
                    user: ['$state', 'Auth', function($state, Auth) {
                        return Auth.getUserOrLogin('root.auth.notifications');
                    }]
                },
                views: {
                    home: {
                        templateUrl: 'templates/auth-notifications.html',
                        controller: 'NotificationsController as notifCtrl'
                    },
                    // adds the user-related entries in the menu
                    'user@root': {
                        templateUrl: 'templates/user-menu.html',
                        controller: 'UserController as userCtrl'
                    }
                }
            })
            ;

            $urlRouterProvider.otherwise('/public');
    }
}());
