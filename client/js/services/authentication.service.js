(function() {
    'use strict';
    angular.module('MixedApis').factory('Auth', AuthFactory);

    AuthFactory.$inject = ['$rootScope', '$http', '$cookies', '$q', '$state', 'User']
    function AuthFactory($rootScope, $http, $cookies, $q, $state, User) {
        // the key associated to the authentication token
        var tokenKey = 'token',
            // flags the connected user
            currentUser;

        // returns the authentication service
        return {
            getToken: getToken,
            setToken: setToken,
            cleanToken: cleanToken,
            login: login,
            logout: logout,
            getUser: getUser,
            getUserOrLogin: getUserOrLogin
        };

        function getToken() {
            return $cookies.get(tokenKey);
        }
        function setToken(token) {
            $cookies.put(tokenKey, token);
        }
        function cleanToken() {
            $cookies.remove(tokenKey);
        }
        // logs the user in and retrieves the user details
        function login(credentials) {
            return $http.post('api/auth/local', {
                login: credentials.login,
                password: credentials.password
            }).then(
                function(loginResponse) {
                    // saves the authentication token and retrieves the user
                    setToken(loginResponse.data.token);
                    return getUser();
                },
                function(errorResponse) {
                    return errorResponse.data;
                }
            );
        }
        // tests whether a user is logged in by attempting to retrieve user details
        function getUser() {
            // if the user is null, attempts to retrieve it from the token and flags the promise until resolution or rejection
            if (!currentUser) {
                var deferredUser = $q.defer();
                currentUser = deferredUser.promise;
                User.get(true).$promise.then(
                    // resolves and flags the user
                    function(userResponse) {
                        deferredUser.resolve(currentUser = userResponse);
                    },
                    // rejects the user
                    function(error) {
                        deferredUser.reject(error);
                        currentUser = null;
                    }
                );

                return currentUser;
            }
            // returns the user promise directly
            else if (currentUser.then) {
                return currentUser;
            }

            // returns the currentUser as a resolved promise otherwise
            return $q.when(currentUser);
        }

        /**
         * Gets the connected user or redirects to the login screen. On authentication,
         * the redirects the user to the onAuthenticatedState routing state
         *
         * @param onAuthenticatedState the route state to display on login
         * @return a promise of the user if connected, a redirection otherwise
         */
        function getUserOrLogin(onAuthenticatedState) {
            return getUser().then(
                function(user) {
                    return user;
                },
                function() {
                    $state.go('root.auth.login', { nextState: onAuthenticatedState });
                }
            );
        }

        // logs the user out and redirects to the home page
        function logout(redirectionState) {
            cleanToken();
            currentUser = null;
            $state.go(redirectionState);
        }
    }
}());
