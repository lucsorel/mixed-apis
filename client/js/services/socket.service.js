(function() {
    'use strict';
    angular.module('MixedApis').factory('SocketService', SocketServiceFactory);

    SocketServiceFactory.$inject = ['$rootScope', '$q', 'Auth']
    function SocketServiceFactory($rootScope, $q, Auth) {
        // holds the promise of a connected socket
        var deferredConnectedSocket = $q.defer();
        // connects with the server and resolves the promise
        var _socket = io.connect('', {
            query: 'token=' + Auth.getToken(),
            path: '/api/socket.io-client/'
        });

        _socket.on('connect', function () {
            deferredConnectedSocket.resolve(_socket);
        });

        function onConnectedSocket() {
            return deferredConnectedSocket.promise;
        }

        return {
            /** emits an event to the server with a facultative callback */
            emit: function(eventName, data, callback) {
                onConnectedSocket().then(function(socket) {
                    socket.emit(eventName, data, function () {
                        if (callback) {
                            var args = arguments;
                            $rootScope.$apply(function () {
                                callback.apply(socket, args);
                            });
                        }
                    });
                });
            },
            on: function(eventName, callback, scope) {
                var handler = null;
                onConnectedSocket().then(function(socket) {
                    // ensures the scope has not been destroyed while waiting for a connected socket
                    if (scope) {
                        // applies the callback in the AngularJS digest cycle
                        handler = function() {
                            var args = arguments;
                            $rootScope.$apply(function () {
                                callback.apply(socket, args);
                            });
                        };

                        socket.on(eventName, handler);
                    }

                    // switches off the server-side events channel and removes the client-side listener
                    scope.$on('$destroy', function() {
                        if (null !== handler) {
                            socket.emit(eventName + ':dispose');
                            socket.removeListener(eventName, handler);
                            handler = null;
                        }
                    });
                });
            }
        };
    }
}());
