(function() {
    'use strict';
    angular.module('MixedApis').factory('TimeseriesService', TimeseriesServiceFactory);

    TimeseriesServiceFactory.$inject = ['$rootScope', '$q', '$http']
    function TimeseriesServiceFactory($rootScope, $q, $http) {
        return {
            getLast: function(howMuch, meters) {
                return $http.get('api/timeseries/last/' + howMuch + '/' + meters.join(','))
                    .then(function(response) {
                        return response.data;
                    });
            }
        };
    }
}());
