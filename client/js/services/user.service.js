(function() {
    'use strict';
    angular.module('MixedApis').factory('User', UserServiceFactory);

    UserServiceFactory.$inject = ['$resource']
    function UserServiceFactory($resource) {
        function extendUser(user) {
            user.isAdmin = function () {
                return this.roles && this.roles.indexOf('admin') !== -1;
            };
            user.isRoot = function () {
                return this.roles && this.roles.indexOf('root') !== -1;
            };
        }

        this.Ressource = $resource('/api/users/:id/:controller', {
            id: '@_id'
        }, {
            get: {
                method: 'GET',
                params: {
                    id: 'me'
                }
                ,interceptor: {
                    response: function (response) {
                        extendUser(response.resource);
                        return response.resource;
                    }
                }
            }
        });
        var self = this;

        self.get = function(force) {
            if (!self.user || force) {
                self.user = self.Ressource.get();
            }

            return self.user;
        };

        return this;
    }
}());
