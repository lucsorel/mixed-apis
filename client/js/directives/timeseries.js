(function() {
    'use strict';
    angular.module('MixedApis')
        .factory('D3Service', D3ServiceFactory)
        .directive('timeseries', Timeseries);

    // d3 provider
    D3ServiceFactory.$inject = ['$window', '$q'];
    function D3ServiceFactory($window, $q) {
        return {
            with: function() {
                return $window.d3 ? $q.when($window.d3) : $q.reject('cannot find d3');
            }
        };
    }

    // timeseries graph directive
    Timeseries.$inject = ['$timeout', 'D3Service'];
    function Timeseries($timeout, D3Service) {
        return {
            restrict: 'E',
            scope: {
                measures: '<'
            },
            templateUrl: 'templates/timeseries.html',
            link: function(scope, element, attrs) {
                // loads d3 and draws the graph
                D3Service.with().then(withD3, console.error);
                var margin = { top: 30, right: 40, bottom: 30, left: 50 },
                    svg, graph, x, waterValueline, electricityValueline;

                // initializes the graph
                function withD3(d3) {
                    // flags the graph holder node and builds the svg child element
                    svg = d3.select(element[0]).select('.timeseries-graph .graph')
                        .append('svg')
                        .attr('width', '100%');

                    // re-renders when data changes
                    scope.$watch('measures', render, true);
                }

                function render() {
                    if (!svg || !scope.measures ) {
                        return;
                    }
                    // remove all previous items before render
                    svg.selectAll('*').remove();

                    var width = 600 - margin.left - margin.right,
                        height = 270 - margin.top - margin.bottom;

                    // sets the ranges
                    x = d3.time.scale().range([0, width]);
                    var y0 = d3.scale.linear().range([height, 0]),
                        y1 = d3.scale.linear().range([height, 0])
                        ;
                    // defines the axes
                    var xAxis = d3.svg.axis().scale(x).orient('bottom').ticks(5),
                        yAxisLeft = d3.svg.axis().scale(y0).orient('left').ticks(5),
                        yAxisRight = d3.svg.axis().scale(y1).orient('right').ticks(5);

                    waterValueline = d3.svg.line()
                        .x(function(measure) { return x(measure.date); })
                        .y(function(measure) { return y0(measure.water); })
                        .interpolate('linear');
                    electricityValueline = d3.svg.line()
                        .x(function(measure) { return x(measure.date); })
                        .y(function(measure) { return y1(measure.electricity); })
                        .interpolate('linear');
                    // restyles the svg canvas
                    graph = svg.attr('width', width + margin.left + margin.right)
                        .attr('height', height + margin.top + margin.bottom)
                        .append('g').attr('transform',
                            'translate(' + margin.left + ',' + margin.top + ')');

                    // adapts the dates and value axes domains
                    x.domain(d3.extent(scope.measures, function(measure) { return measure.date; }));
                    y0.domain([0, d3.max(scope.measures, function(measure) { return measure.water; })]);
                    y1.domain([0, d3.max(scope.measures, function(measure) { return measure.electricity; })]);

                    // adds the water and electricity paths
                    graph.append('path').attr('class', 'water').attr('d', waterValueline(scope.measures))
                        .style('stroke', 'red');
                    graph.append('path').attr('class', 'electricity').attr('d', electricityValueline(scope.measures));
                    // adds the X Axis
                    graph.append('g')
                        .attr('class', 'x axis')
                        .attr('transform', 'translate(0,' + height + ')')
                        .call(xAxis);
                    // adds the measures axes
                    graph.append('g')
                            .attr('class', 'y axis')
                            .style('fill', 'steelblue')
                            .call(yAxisLeft)
                        .append('text')
                            .attr('transform', 'rotate(-90)')
                            .attr('y', 6)
                            .attr('dy', '.71em')
                            .style('text-anchor', 'end')
                            .text('Water (m³/s)');
                        ;
                    graph.append('g')
                            .attr('class', 'y axis')
                            .attr('transform', 'translate(' + width + ' ,0)')
                            .style('fill', 'red')
                            .call(yAxisRight)
                        .append('text')
                            .attr('transform', 'rotate(-90)')
                            .attr('y', -19)
                            .attr('dy', '.71em')
                            .style('text-anchor', 'end')
                            .text('Electricity (W)')
                        ;
                }
            }
        };
    }
}());
