(function() {
    'use strict';
    angular.module('MixedApis').controller('UserController', UserController);

    UserController.$inject = ['Auth']
    function UserController(Auth) {
        // flags the user
        var userCtrl = this;
        Auth.getUser().then(function(user) {
            userCtrl.user = user;
        });

        // logs the user out and redirects to the specified page
        this.logout = function() {
            Auth.logout('root.public');
        };
    }
}());
