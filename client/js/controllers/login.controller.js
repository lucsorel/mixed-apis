(function() {
    'use strict';
    angular.module('MixedApis').controller('LoginController', LoginController);

    LoginController.$inject = ['$state', '$stateParams', 'Auth', 'TimeseriesService'];
    function LoginController($state, $stateParams, Auth, TimeseriesService) {
        var loginCtrl = this;

        loginCtrl.authenticating = false;

        loginCtrl.authenticate = function() {
            if (!loginCtrl.authenticating) {
                loginCtrl.authenticating = true;
                Auth.login(loginCtrl.credentials).then(
                    function() {
                        loginCtrl.authenticating = false;
                        // redirects to the authenticated page on success
                        $state.go($stateParams.nextState);
                    },
                    console.log);
            }
        };
    }
}());
