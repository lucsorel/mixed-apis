(function() {
    'use strict';
    angular.module('MixedApis').controller('MenuController', MenuController);

    MenuController.$inject = ['menuService'];
    function MenuController(menuService) {
        // binds the menuService's page to the menu controller
        this.page = menuService.page;
    }
}());
