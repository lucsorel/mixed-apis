(function() {
    'use strict';
    angular.module('MixedApis').controller('TimeseriesController', TimeseriesController);

    TimeseriesController.$inject = ['$scope', '$q', 'TimeseriesService', 'D3Service', 'SocketService']
    function TimeseriesController($scope, $q, TimeseriesService, D3Service, SocketService) {
        var tsCtrl = this,
            dateParser,
            entrySetMapper = function(entriesSet) {
                return {
                    date: dateParser(entriesSet[0].date),
                    water: +entriesSet[0].quantity,
                    electricity: +entriesSet[1].quantity
                };
            };

        // retrieves d3 and the last 10 measures
        $q.all([
            D3Service.with(),
            TimeseriesService.getLast(10, ['water', 'electricity'])
        ])
        // reformats the measures
        .then(function(responses) {
            dateParser = responses[0].time.format('%Y-%m-%dT%H:%M:%S.%LZ').parse;
            return responses[1].map(entrySetMapper);
        })
        // views the measures via the controller and subscribes to the notifications
        .then(function(measures) {
            tsCtrl.measures = measures;

            // registers a handler on the timeseries notification channel and subscribes to it
            SocketService.on('timeserie:notify', function(entriesSet) {
                // removes the oldest value, adds the newest one
                tsCtrl.measures.shift();
                tsCtrl.measures.push(entrySetMapper(entriesSet));
            }, $scope);

            SocketService.emit('timeserie:notify:subscribe');
        });
    }
}());
