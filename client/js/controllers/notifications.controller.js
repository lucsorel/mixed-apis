(function() {
    'use strict';
    angular.module('MixedApis').controller('NotificationsController', NotificationsController);

    NotificationsController.$inject = ['$scope', '$q', 'SocketService']
    function NotificationsController($scope, $q, SocketService) {
        var notifCtrl = this;
        notifCtrl.notifications = [];

        notifCtrl.subscribe = function() {
            SocketService.emit('notification:notify:subscribe');
        };

        notifCtrl.dispose = function() {
            SocketService.emit('notification:notify:dispose');
        };

        // registers a handler on the notification channel and subscribes to it
        SocketService.on('notification:notify', function(notification) {
            // console.log(notification);
            // keeps the 5 most recent notifications
            notifCtrl.notifications.push(notification);
            if (notifCtrl.notifications.length > 5) {
                notifCtrl.notifications.shift();
            }
        }, $scope);
        notifCtrl.subscribe();
    }
}());
