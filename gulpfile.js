'use strict';

// gulp and dependencies
var gulp = require('gulp')
    ,gutil = require('gulp-util')
     // allows to clean the local www folder
    ,del = require('del')
    // drives minifications and replacements in html
    ,usemin = require('gulp-usemin')
    // suffixes files with a hash context
    ,rev = require('gulp-rev')
    // css minification
    ,cleanCss = require('gulp-clean-css')
    // js uglification
    ,uglify = require('gulp-uglify')
    // minifies and caches html templates
    ,htmlmin = require('gulp-htmlmin')
    ,templateCache = require('gulp-angular-templatecache')
    // in order to run spritification and packaging in series (parallel is the gulp default behavior)
    , runSequence = require('run-sequence');

/** detects whether minification processes are expected (skipped to make dev faster) */
function shouldMinify() {
    return process.argv.indexOf('--no-minify') < 0;
}

/** cleans the local www compilation folder */
gulp.task('clean-www', function() {
    return del(['www/**/*']);
});

/** cleans the local client/gen/js/templates.js file */
gulp.task('clean-gen-templates', function() {
    return del(['client/gen/js/templates.js']);
});

/** concatenates the html files into a generated templates.js that populates Angular's $cacheTemplate */
gulp.task('cache-ng-templates', ['clean-gen-templates'], function() {
    return gulp.src('client/templates/*.html')
        .pipe(shouldMinify() ? htmlmin({ collapseWhitespace: true }) : gutil.noop())
        .pipe(templateCache('templates.js', { module: 'MixedApis', root: 'templates' }))
        .pipe(gulp.dest('client/gen/js'));
});

/** copies the glyphicons fonts in the www folder */
gulp.task('copy-fonts', function() {
    return gulp.src('node_modules/bootstrap/fonts/*.*')
        .pipe(gulp.dest('www/fonts'));
});

/**
 * Builds the mixed-APIs files:
 * - cleans the local folders
 * - minifies the resources
 * - copies the compiled resources in the local www folder
 */
gulp.task('package-mixed-apis', ['clean-www'], function() {
    var cssPreProcessors, jsPreProcessors;

    // defines the CSS and JS pre-processors depending on whether minification is expected
    if (shouldMinify()) {
        cssPreProcessors = [cleanCss, rev];
        jsPreProcessors = [uglify, rev];
    }
    else {
        cssPreProcessors = [rev];
        jsPreProcessors = [rev];
    }

    return gulp.src('client/index.html')
        // processes CSS & JS resources
        .pipe(usemin({
            css: cssPreProcessors,
            js: jsPreProcessors
        }))
        // writes the output in the local www folders
        .pipe(gulp.dest('www/'));
});

/**
* Watches for local changes and packages the mixed-APIs files
* - packages the index.html and its CSS & JS resources
* - watches for CSS & JS resources changes
*/
gulp.task('_watch', function() {
    // watches changes in the raw html templates
    var watchedTemplates = ['client/templates/*.html'],
        // watches changes in the CSS and JS resources
        watchedResources = [
            'client/index-v3.html',
            'client/css/*.css',
            'client/js/**/*.js',
            'client/gen/js/*.js'
        ];

    // templates caching will generate a new client/gen/templates.js, which will trigger the package-mixed-apis watch
    gulp.watch(watchedTemplates, ['cache-ng-templates']);

    // packaging will include the sprites.css in its build and copy the webapp/gen/img/sprites.png in the webapp
    gulp.watch(watchedResources, ['package-mixed-apis']);
});

/** ensures dependent tasks run in series before actually watching changes */
gulp.task('watch', function(completionCallback) {
    runSequence('build', '_watch', completionCallback);
});

/** full-build task */
gulp.task('build', function(completionCallback) {
    runSequence('cache-ng-templates', 'package-mixed-apis', 'copy-fonts', completionCallback);
});
