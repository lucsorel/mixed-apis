// retrieves the user credentials from the arguments
if (process.argv.length < 4) {
    console.log('missing environment argument. Execute this script with `node scripts/add-user.js {user-login} {user-password}` or `npm run add-user {user-login} {user-password}`');
    process.exit(1);
}
var login = process.argv[2],
    password = process.argv[3]

// connects to the database
var config = require('../server/config/environment'),
    mongoose = require('mongoose'),
    User = require('../server/api/user/user.model'),
    connection = mongoose.connect(config.mongo.uri, config.mongo.options).connection
    ;
if (connection) {
    connection.on('error', console.log);
}

User.create({
    login: login,
    password: password,
    roles: ['user']
}).then(
    function(user) {
        console.log('user created:');
        console.log(user);
        process.exit(0);
    },
    function(error) {
        console.log('failed to create user:');
        console.log(error);
        process.exit(1);
    }
);
