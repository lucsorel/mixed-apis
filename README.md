# Purpose
Demonstrate the use of [RxJS observables](http://reactivex.io/documentation/observable.html) to let the front-end app decide what events should be sent by the server through websockets (secured via JSON web tokens).

# Install
Requirements:
* it is a **NodeJS** application
* users and their passwords are stored in a **MongoDB** database

Installation:
```bash
# checkout the project
git clone https://gitlab.com/lucsorel/mixed-apis.git

# install dependencies
cd mixed-apis
npm i

# add test users `npm run add-user {user-login} {user-password}`
npm run add-user harrycover 123456
npm run add-user nasredine h0dj4

# build the front-end app and starts the server
npm run build-no-minify && npm start
```

# Use case
* create 2 users
* in 2 different browsers, authenticate with a different user
* authenticate and go to the notification screen
* each user will receive only the notifications meant for him
* if someone disposes from the notifications, it does not affect the ones sent to the other user
* if both users dispose from the notifications, the server stops emitting notifications
