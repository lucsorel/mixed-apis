/**
 * User: Erwan Daubert - erwan.daubert@gmail.com
 * Date: 06/08/15
 * Time: 09:41
 *
 * @author Erwan Daubert
 * @version 1.0
 */

var Stream = require('stream').Stream;

var StreamFormatter = function () {
    Stream.call(this);
    this.writable = true;
    this._done = false;
    this._empty = true;
};

StreamFormatter.prototype.__proto__ = Stream.prototype;

StreamFormatter.prototype.write = function (doc) {
    if (!this._hasWritten) {
        this._hasWritten = true;
        this._empty = false;

        // open an object literal / array string along with the doc
        this.emit('data', '[' + doc);
    }
    else {
        this.emit('data', ',' + doc);
    }

    return true;
};

StreamFormatter.prototype.end =
    StreamFormatter.prototype.destroy = function () {
        if (this._done) {
            return;
        }
        if (this._empty) {
            this.emit('data', '[');
        }
        this._done = true;

        // close the object literal / array
        this.emit('data', ']');
        // done
        this.emit('end');
    };

module.exports = StreamFormatter;
