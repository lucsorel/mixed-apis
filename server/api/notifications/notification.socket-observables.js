'use strict';

/**
 * Exposes the observables (event channels) concerning notifications that should
 * be sent to a user
 **/

/* <the code of this section aims at creating fake notifications> */
var userIds = [],
    notificationIndex = 0,
    // builds a fake notification
    fakeNotificationBuilder = function() {
        notificationIndex++;
        var notification = {
            title: 'Notification n°' + notificationIndex,
            meter: 'electricity',
            access: {
                // randomly picks a user id
                users: [ userIds[Math.floor(userIds.length * Math.random())] ]
            }
        };
        // console.log(notification);
        return notification;
    },
    // connects to the database to retrieve some ids of real users
    config = require('../../config/environment'),
    mongoose = require('mongoose'),
    User = require('../user/user.model'),
    connection = mongoose.connect(config.mongo.uri, config.mongo.options).connection
    ;
if (connection) {
    connection.on('error', console.log);
}
User.find({}).limit(10).then(function(users) {
    users.forEach(function(user) {
        userIds.push(user._id.toString());
    });
});
/* </the code of this section aims at creating fake notifications> */

// the real-life code starts here
var Notification = require('./notification.model'),
    Rx = require('rx'),
    saveObservable = new Rx.Subject(),
    removeObservable = new Rx.Subject(),
    // fakes notifications to send to the users every 3 seconds
    notifyObservable = Rx.Observable.interval(3000).map(
        fakeNotificationBuilder
    ).publish().refCount();

// pushes new and removed notifications to their dedicated subjects
Notification.schema.post('save', saveObservable.onNext);
Notification.schema.post('remove', removeObservable.onNext);

// exposes the notification observables and a filter function defining whether an notification should be sent to the given user
module.exports = {
    package: 'notification',
    events: {
        save: saveObservable,
        remove: removeObservable,
        notify: notifyObservable
    },

    // defines whether the given notification concerns the given user
    userFilter: function(notification, user) {
        // console.log('testing ' + notification.title + ' with user ' + user._id + ': ' + (notification && user && (notification.access.users.indexOf(user._id) > -1)));
        return notification && user && (notification.access.users.indexOf(user._id) > -1);
    }
};
