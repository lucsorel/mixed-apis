'use strict';

var Timeserie = require('./timeserie.model'),
    Rx = require('rx'),
    // fakes timeseries entries to send to the users every 3 seconds (would also be a Subject in real life)
    notifyObservable = Rx.Observable.interval(3000).map(
        function() {
            return Timeserie.generateMeters(['water', 'electricity']);
        }
    ).publish().refCount();

// exposes the notification observables and a filter function defining whether an notification should be sent to the given user
module.exports = {
    package: 'timeserie',
    events: {
        notify: notifyObservable
    },

    // here, all users can receive all timeseries
    userFilter: function(timeserie, user) {
        return true;
    }
};
