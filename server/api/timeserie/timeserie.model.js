'use strict';

var Readable = require('stream').Readable,
    util = require('util'),
    // date used to start the meters dataset and incremented at each call
    startDate = new Date();

util.inherits(MetersData, Readable);

function MetersData(meters, howMuch) {
    Readable.call(this);
    this.meters = meters;
    this.howMuch = howMuch;
    this.index = 0;
}

function generateMeters(meters) {
    // clones from the global date so that increments do not impact the meters date
    var meterDate = new Date(startDate.getTime()),
        metersData = meters.map(function(meter) {
        return {
            meter: meter,
            date: meterDate,
            // generates a cosine-series value
            quantity: meter.length + Math.cos(meterDate.getTime() / (10000*meter.length))
        };
    });

    // adds an hour to the start date for the next computation
    startDate.setTime(startDate.getTime() + 60000);

    // returns the meters
    return metersData;
}

MetersData.prototype._read = function() {
    var metersData;
    // builds and pushes the meters dataset
    if (this.index < this.howMuch) {
        var metersData = generateMeters(this.meters);
        this.push(new Buffer(JSON.stringify(metersData), 'utf8'));
        // increments the index for the next dataset
        this.index++;
    }
    // triggers the end of the stream
    else {
        this.push(null);
    }
};

module.exports = {
    generateMeters: generateMeters,
    timeseries: {
        // streams the last entries
        findLast: function (meters, howMuch) {
            return new MetersData(meters, howMuch);
        }
    }
};
