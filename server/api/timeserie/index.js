'use strict';

var express = require('express');
var controller = require('./timeserie.controller.js');
var auth = require('../../auth/auth.service');

var router = express.Router();

// TODO add a new level of authentication which check if the role is allowed to look for the meters
// router.get('/dates/:start/:end/:meters', auth.isAuthenticated(), controller.getData);
router.get('/last/:howMuch/:meters', auth.isAuthenticated(), controller.getLast);
// router.put('/', auth.isAuthenticated(), controller.update);
// router.post('/', controller.create);

module.exports = router;
