'use strict';

var Timeserie = require('./timeserie.model.js').timeseries,
    StreamFormatter = require('../../components/stream/StreamFormatter');
    ;

function handleError(res, err) {
    console.log(err);
    return res.status(500).send(err);
}

exports.getLast = function(req, res) {
    var howMuch = req.params.howMuch || 1,
        meters = decodeURIComponent(req.params.meters).split(',');

    res.append('content-type', 'application/json');
    Timeserie.findLast(meters, howMuch)
        .on('error', function (e) {
            handleError(res, e)
        })
        // formats the stream into a JSON document written to the response
        .pipe(new StreamFormatter())
        .on('error', function (e) {
            handleError(res, e)
        })
        .pipe(res)
        .on('error', function (e) {
            handleError(res, e)
        });
};
