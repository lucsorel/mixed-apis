'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    crypto = require('crypto');

var UserSchema = new Schema({
    login: {
        type: String,
        required: true,
        unique: true,
        index: true
    },
    password: {
        type: String,
        required: true
    },
    salt: {
        type: String,
        required: false
    },
    email: {
        type: String,
        required: false
    },
    roles: {
        type: Array,
        required: false,
        default: 'user'
    }
});

// Non-sensitive info we'll be putting in the token
UserSchema
    .virtual('token')
    .get(function() {
        return {
            '_id': this._id,
            'roles': this.roles
        };
    });

// validates empty password
UserSchema
    .path('password')
    .validate(function(hashedPassword) {
        return hashedPassword.length;
    }, 'Password cannot be blank');

var validatePresenceOf = function(value) {
    return value && value.length;
};
// ensures password is defined
UserSchema
  .pre('save', function(next) {
    if (!this.isNew) {
        return next();
    }

    if (!validatePresenceOf(this.password)) {
        next(new Error('Invalid password'));
    } else {
        next();
    }
  });
// generates salt
UserSchema
    .pre('save', function(next) {
        if (!this.isNew) {
            return next();
        }

        if (!this.salt) {
            this.salt = this.makeSalt();
        }
        next();
    });

// encrypts password
UserSchema
    .pre('save', function(next) {
        if (!this.isNew) {
            return next();
        }
        this.password = this.encryptPassword(this.password);
        next();
    });

UserSchema.methods = {
    /**
    * Authenticate - check if the passwords are the same
    *
    * @param {String} plainText
    * @return {Boolean}
    * @api public
    */
    authenticate: function(plainText) {
        return this.encryptPassword(plainText) === this.password;
    },

    /**
    * Make salt
    *
    * @return {String}
    * @api public
    */
    makeSalt: function() {
        return crypto.randomBytes(16).toString('base64');
    },

    /**
    * Encrypt password
    *
    * @param {String} password
    * @return {String}
    * @api public
    */
    encryptPassword: function(password) {
    if (!password || !this.salt) {
        return '';
    }
    var salt = new Buffer(this.salt, 'base64');
    return crypto.pbkdf2Sync(password, salt, 10000, 64).toString('base64');
    }
};

module.exports = mongoose.model('User', UserSchema);
