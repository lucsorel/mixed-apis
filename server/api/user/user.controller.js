var User = require('./user.model');

module.exports = {
    me: function (req, res, next) {
        var userId = req.user._id;
        User.findOne({
            _id: userId
        }, '-salt -password', function (err, user) {
            if (err) {
                next(err);
            }
            else if (!user) {
                res.json(401);
            }
            else {
                res.json(user);
            }
        });
    }
};
