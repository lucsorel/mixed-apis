'use strict';

var router = require('express').Router()
    ,auth = require('../../auth/auth.service')
    ,controller = require('./user.controller')
    ;

router.get('/me', auth.isAuthenticated(), controller.me);

module.exports = router;
