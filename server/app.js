// defaults to 'development' environment and 3033 http port
process.env.NODE_ENV = /*process.env.NODE_ENV ||*/ 'development';
process.env.PORT = /*process.env.PORT ||*/ 3033;

// application dependencies
var config = require('./config/environment')
    ,mongoose = require('mongoose')
    ,express = require('express')
    ,switchAuth = express()
    ,http = require('http').Server(switchAuth)
    ,bodyParser = require('body-parser')
    ,cookieParser = require('cookie-parser')
    ,passport = require('passport')
    ,socketio = require('socket.io')(http, {
        serveClient: true,
        path: '/api/socket.io-client/'
    })
    ;

// connects to the database
var connection = mongoose.connect(config.mongo.uri, config.mongo.options).connection;
if (connection) {
    connection.on('error', console.log);
}

// configures the webapp
switchAuth.disable('x-powered-by');
switchAuth.use(express.static(__dirname + '/../www'));
switchAuth.use(bodyParser.json({ limit: '1mb' }));
switchAuth.use(bodyParser.urlencoded({ limit: '1mb', extended: false }));

// authentication settings
switchAuth.use(cookieParser());
switchAuth.use(passport.initialize());

// HTTP pages routes and API endpoints
require('./routes')(switchAuth);

// websocket API
require('./config/socketio')(socketio);

// starts the web aplication server on the configured HTTP port
http.listen(process.env.PORT, function() {
    console.log('listening on *:' + process.env.PORT + '\nctrl+c to stop the app');
});

// shuts the application down on low-level errors
function shutdown() {
    console.log('Mixed-APIs is shutting down...');
    process.exit(1);
};
process.on('SIGINT', shutdown).on('SIGTERM', shutdown);
