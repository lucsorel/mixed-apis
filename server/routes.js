/**
 * Main application routes
 */
'use strict';

module.exports = function (app) {
    app.enable('trust proxy');
    // Insert routes below
    // app.use('/api/reports', require('./api/report'));
    // app.use('/api/actions', require('./api/action'));
    // app.use('/api/alerts', require('./api/alert'));
    // app.use('/api/index-history', require('./api/index'));
    // app.use('/api/c3', require('./api/c3'));
    // app.use('/api/comments', require('./api/comment'));
    // app.use('/api/chart-configurations', require('./api/chartConfiguration'));
    // app.use('/api/units', require('./api/unit'));
    // app.use('/api/data-types', require('./api/data-type'));
    // app.use('/api/perimeter-global', require('./api/perimeter-global'));
    // app.use('/api/perimeter', require('./api/perimeter'));
    // app.use('/api/events', require('./api/event'));
    // app.use('/api/imports', require('./api/import'));
    // app.use('/api/contracts', require('./api/contract'));
    // app.use('/api/configurations', require('./api/configuration'));
    app.use('/api/timeseries', require('./api/timeserie'));
    app.use('/api/users', require('./api/user'));
    // app.use('/api/init', require('./api/init'));
    app.use('/api/auth', require('./auth'));

    // All undefined api routes should return a 404
    app.route('/api/*').get(function pageNotFound(req, res) {
        res.status(404).send({message: req.url + " is not handled by the API!"});
    });

    // All other routes should redirect to the index.html
    app.route('/*')
        .get(function (req, res) {
            res.sendFile('index.html', {
                root: app.get('appPath') + '/'
            });
        });
};
