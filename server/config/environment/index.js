var config = {
    env: process.env.NODE_ENV,
    port: process.env.PORT,
    secrets: {
        session: 'session-secret'
    },
    mongo: {
        uri: 'mongodb://localhost/switch-auth',
        options: {
            db: {
                safe: true
            },
            sever: {
                auto_reconnect: true
            }
        }
    }
};

module.exports = config;
