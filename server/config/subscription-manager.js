'use strict';

// prevents NPEs when disposing twice from an subscription
function safeDispose(subscriptions, key) {
    if (subscriptions && subscriptions.hasOwnProperty(key)) {
        subscriptions[key].dispose();
        delete subscriptions[key];
    }
}

/**
 * Allows the given authenticated socket to subscribe to system event channels
 * and sends events (referring to the authenticated user) to the browser
 *
 * @param socket the authenticated socket
 * @param connectedUser the authenticated user
 * @param socketObservables a set of observables from the same package to which the socket can subscribe and dispose
 */
module.exports = function(socket, connectedUser, socketObservables) {
    var userFilter = socketObservables.userFilter,
        // caches the subscriptions of the authenticated socket for the given observables package
        subscriptions = {},
        eventFullName;

    // iterates over the event channels to add the ':subscribe' and ':dispose' handlers
    Object.keys(socketObservables.events).forEach(function(eventName) {
        // builds the '{package}:{event}' event fullname
        var eventFullName = socketObservables.package + ':' + eventName;

        // subscribes to the observable of the event channel and sends the filtered events to the browser
        socket.on(eventFullName + ':subscribe', function() {
            // prevents from subscribing twice to a channel
            if (!subscriptions.hasOwnProperty(eventFullName)) {
                // subscribes the socket to the events referring to the connected user
                subscriptions[eventFullName] = socketObservables.events[eventName]
                    .flatMap(
                        function(event) {
                            return userFilter(event, connectedUser) ? [event] : [];
                        }
                    )
                    .subscribe(
                        function(event) {
                            socket.emit(eventFullName, event);
                        });
            }
        });

        // disposes the '{package}:{event}' subscription on '{package}:{event}:dispose' event
        socket.on(eventFullName + ':dispose', function() {
            safeDispose(subscriptions, eventFullName);
        });
    });

    // disposes from all subscriptions on disconnection
    socket.on('disconnect', function() {
        Object.keys(subscriptions).forEach(function(key) {
            safeDispose(subscriptions, key);
        });
    });
}
