'use strict';

var config = require('./environment'),
    socketIoJwt = require('socketio-jwt'),
    subscriptionManager = require('./subscription-manager')
    ;
// When the user disconnects.. perform this
function onDisconnect(socket) {
    console.log('socket disconnected');
}

// When the user connects.. perform this
function onConnect(socket, connectedUser) {
    // When the client emits 'info', this listens and executes
    socket.on('info', function (data) {
        console.info('[%s] %s', socket.address, JSON.stringify(data, null, 2));
    });

    // allows the authenticated socket to subscribe the notification channels
    subscriptionManager(socket, connectedUser, require('../api/notifications/notification.socket-observables'));
    subscriptionManager(socket, connectedUser, require('../api/timeserie/timeserie.socket-observables'));
}

module.exports = function (socketio) {
    // connects only to authenticated users
    socketio.use(socketIoJwt.authorize({
        secret: config.secrets.session,
        handshake: true
    }));

    socketio.on('connection', function (socket) {
        // Call onDisconnect
        socket.on('disconnect', function () {
            onDisconnect(socket);
            console.info('[%s] DISCONNECTED', socket.request.socket.remoteAddress);
        });

        // Call onConnect
        onConnect(socket, socket.decoded_token);
        console.info('[%s] CONNECTED', socket.request.socket.remoteAddress);
    });
};
