'use strict';

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

exports.setup = function (User, config) {
    passport.use(new LocalStrategy({
            usernameField: 'login',
            passwordField: 'password' // this is the virtual field on the model
        },
        function(login, password, done) {
            User.findOne({
                login: login
            }, function(err, user) {
            if (err) {
                return done(err);
            }
            if (!user) {
                return done(null, false, { code: 0 });
            }
            if (!user.authenticate(password)) {
                return done(null, false, { code: 1 });
            }
            return done(null, user);
            });
        }
    ));
};
