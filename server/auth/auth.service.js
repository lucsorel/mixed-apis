'use strict';

var config = require('../config/environment')
    ,jwt = require('jsonwebtoken')
    ,compose = require('composable-middleware')
    ,User = require('../api/user/user.model')
    ,expressJwt = require('express-jwt')
    ,tokenCookieRegExp = /token=([^;]+)/
    ,validateJwt = expressJwt({
        secret: config.secrets.session,
        // gets the authentication token from the cookies
        getToken: function getToken(req) {
            if (req && req.headers && req.headers.cookie) {
                var matches = req.headers.cookie.match(tokenCookieRegExp);
                if (matches && matches.length > 1) {
                    return matches[1];
                }
            }

            return null;
        }
    })
    ;

/**
 * Attaches the user object to the request if authenticated
 * Otherwise returns 403
 */
function isAuthenticated() {
    return compose()
        // Validate jwt
        .use(function (req, res, next) {
            // // allow access_token to be passed through query parameter as well
            // if (req.query && req.query.hasOwnProperty('access_token')) {
            //     req.headers.authorization = 'Bearer ' + req.query.access_token;
            // }
            validateJwt(req, res, next);
        })
        // attaches the user to request
        .use(function (req, res, next) {
            User.findById(req.user._id, function (err, user) {
                if (err || !user) {
                    return res.status(401).end();
                }

                req.user = user;
                next();
            });
        });
}
/**
 * Returns a jwt token signed by the app secret
 */
function signToken(id, roles, keepMeLoggedIn) {
    if (keepMeLoggedIn) {
        return jwt.sign({_id: id, roles: roles}, config.secrets.session);
    }
    else {
        return jwt.sign({_id: id, roles: roles}, config.secrets.session, {ExpiresIn: 30*60});
    }
}

/**
 * Set token cookie directly for oAuth strategies
 */
function setTokenCookie(req, res) {
    if (!req.user) {
        return res.json(404, { message: 'Something went wrong, please try again.'});
    }
    var token = signToken(req.user._id, req.user.roles, req.user.stayConnected);
    res.cookie('token', JSON.stringify(token));
    res.redirect('/');
}

exports.isAuthenticated = isAuthenticated;
exports.signToken = signToken;
exports.setTokenCookie = setTokenCookie;
